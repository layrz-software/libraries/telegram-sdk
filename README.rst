============
Telegram SDK
============

Managed by Golden M, Inc.

Description
~~~~~~~~~~~
It's an implementation of a simple and intuitive SDK wrapper for Telegram API.

Works with the Telegram public API or a private Telegram API.

How to contribute
~~~~~~~~~~~~~~~~~
The instructions for contributing to this project are available at `CONTRIBUTING.rst`

Work with us
~~~~~~~~~~~~
Golden M is a software/hardware development company what is working on
a new, innovative and disruptive technologies.

For more information, contact us at `sales@goldenmcorp.com <mailto:sales@goldenmcorp.com>`_

License
~~~~~~~
This project is under MIT License, for more information, check out the `LICENCE`
